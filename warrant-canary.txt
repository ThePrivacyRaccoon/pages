The Privacy Raccoon's Warrant Canary
====================================

If you do not know what a Warrant Canary is, take a look at: 
https://en.wikipedia.org/wiki/Warrant_canary

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

As of 2022-09-04, The Privacy Raccoon website content has not been 
compromised due to any warrant by a three-letter agency, government
or any other entity.

Today New York Times headlines:

- - Abortion Pill Providers Experiment With Ways to Broaden Access
- - How a Record Cash Haul Vanished for Senate Republicans
- - How Fake GPS Coordinates Are Leading to Lawlessness on the High Seas
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEE67dPoDnSt5DKUpIoRE+oKtmQm7cFAmMUUjYACgkQRE+oKtmQ
m7dfKBAAyakNksa1Q1oYITEi+7Izi3n3OblSgDot0OwE+wJFl7YO1Vy2uSELxePP
W1cC1/hliIvnEzfXmjjRlGV1Ze6hdR4pLaaFjFsvzD3NSwiupD850TgGtChdZds/
jdqzy/0oloRrdnQdtIDLlmxJHW8S4+tooNER8ynuZWcp9Eid1iqeBMch66VV6nOV
OynfhYas0xlaFupfESX8P8raNnDoFo+XFIYhWYGanMCV3jmOjDqjfQKQimhf142g
YB6S2hIrWoymcM8AcBXzeZRme+xcMwpmUYDdTLQk9bun07V0GlAriFhp3No8BkS8
GZ2GJftWoKjNoKYN33uFVEnPSiPqxpZ8rUdWdnymKAUVdZ6bNn78tvoMAaejF73G
kbHQB9a4979/GSpynSAj87hNvVawtWhyfcEgynZjX+xbnvjqWKnkwLep+hkKfjJ7
+qQo8mqIdDdarvdWAzwiE2CssZyMvXCHiyAYavJiWQ4MlvXsKEtAEBX788xqZDoX
t5cGPpsz7myWlZ0NmHo0wAq3Fm7d2mrYBFO7+64dCw8EJ7BMtcUBcVJh9WPwQxfY
uWgR7jW3hVqV7kM22c6rlQZeR1bJHbWJsVK/VgFV4mtIQvLbWTTaABasaBdgl5fs
iaDPurzM6CFRo3NdjHMhBpFs/+ZGj6IhR+Yn+ePV+x/tUBrtRk4=
=pgyZ
-----END PGP SIGNATURE-----

Why is the Warrant Canary important?
====================================

Because we're dedicated to protecting everyone's privacy, including
those prosecuted by opressive governments. Without a Warrant Cannary,
there would be no confirmation that a government or other malicious
actor has forced us to recommend software and/or services that might
compromise our readers privacy.

The content is cryptographically signed using the OpenPGP standard
to confirm that it has been written by The Privacy Raccoon maintainers
and not any impostor.

What is the content in the Warrant Canary?
==========================================

We clearly state that our recommendations/guides/articles have not been
compromised by any warrant from governmentes of any corporation interests.

We also include headlines from a popular news source, which verify that
the Warrant Canary could not have been written before the given date.

How frequently do you update the Warrant Canary?
================================================

We'd like to update it at least once every three months. If three months have
passed, don't panic, we may have just forgotten to update it! Send us an email.

The clear signal that the website has been compromised would be the removal 
of the Warrant Canary without previous announcement.

The keys
========

Currently, messages are signed by the main maintainer of the site keys, Werwolf.
You can find Werwolf's public key here:

https://blackgnu.net/pages/keys.txt
